import 'package:flutter/material.dart';
import 'package:stripe_payment/stripe_payment.dart';
import 'dart:async';
import 'package:flutter_learn/bloc/bloc_provider.dart';

import 'package:flutter_learn/bloc/application_bloc.dart';
import 'package:flutter_learn/screens/local_bloc_screen.dart';

class FutureBuilderList extends StatelessWidget {

  Future getProjectDetails() async {
    //var result = await http.get('https://getProjectList');
    //result is array of objects for list
    //return result;


    return null;
  }



  Widget projectWidget() {
    return FutureBuilder(
      builder: (context, projectSnap) {
        if (projectSnap.connectionState == ConnectionState.none &&
            projectSnap.hasData == null) {
          //print('project snapshot data is: ${projectSnap.data}');
          return Container();
        }
        return ListView.builder(
          itemCount: projectSnap.data.length,
          itemBuilder: (context, index) {
            //ProjectModel project = projectSnap.data[index];
            return Column(
              children: <Widget>[
                // Widget to display the list of project
              ],
            );
          },
        );
      },
      future: getProjectDetails(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ProjectList'),
      ),
      body: projectWidget(),
    );
  }

}

//to add
// bloc.incrementCounter.add(null);