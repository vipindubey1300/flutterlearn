import 'package:flutter/material.dart';
import 'package:flutter_learn/widgets/list_item.dart';
import 'package:stripe_payment/stripe_payment.dart';
import 'dart:async';
import 'package:flutter_learn/bloc/bloc_provider.dart';

import 'package:flutter_learn/api/api_response.dart';
import 'package:flutter_learn/screens/local_bloc_screen.dart';
import 'package:flutter_learn/bloc/movie_bloc.dart';
import 'package:flutter_learn/models/models_list.dart';



class ModelsScreen extends StatefulWidget {
  @override
  _ModelsScreenState createState() => _ModelsScreenState();
}

class _ModelsScreenState extends State<ModelsScreen> {
  ModelsBloc _bloc;

  @override
  void initState() {
    super.initState();
    _bloc = ModelsBloc();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Movie Mania')),
      backgroundColor: Colors.white,
      body: RefreshIndicator(
        onRefresh: () => _bloc.fetchMovieList(),
        child: StreamBuilder<ApiResponse<List<Models>>>(
          stream: _bloc.movieListStream,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              switch (snapshot.data.status) {
                case Status.LOADING:
//                  return Loading(
//                    loadingMessage: snapshot.data.message,
                  print('LOADING-----------------');

//                  );
                  break;
                case Status.COMPLETED:
                 // return MovieList(movieList: snapshot.data.data)
                // re;
                return ListView.builder(
                  itemCount: snapshot.data.data.length,
                  itemBuilder: (context, index) {
                    //ProjectModel project = projectSnap.data[index];
                    return Column(
                      children: <Widget>[
                        // Widget to display the list of project
                         ModelsTile(model: snapshot.data.data[index])
                      ],
                    );
                  },
                );
                print('COMPLETED-----------------');
                  break;
                case Status.ERROR:
                  print('ERROR-----------------');

//                  return Error(
//                    errorMessage: snapshot.data.message,
//                    onRetryPressed: () => _bloc.fetchMovieList(),
//                  );
                  break;
              }
            }
            return Container();
          },
        ),
      ),
    );
  }

  @override
  void dispose() {
    _bloc.dispose();
    super.dispose();
  }
}