import 'package:flutter/material.dart';
import 'package:stripe_payment/stripe_payment.dart';
import 'dart:async';
import 'package:flutter_learn/bloc/bloc_provider.dart';
import 'package:flutter_learn/bloc/counter_bloc.dart';
//Below uses bloc is used by a single page
class LocalBlocForScreen extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    // 1
    final bloc = CounterBloc();

    // 2
    return BlocProvider<CounterBloc>(
      bloc: bloc,
      child: Scaffold(
        appBar: AppBar(title: Text('Where do you want to eat?')),
        body: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: TextField(
                decoration: InputDecoration(
                    border: OutlineInputBorder(), hintText: 'Enter a location'),

                // 3
                onChanged: (query) => bloc.handleLogic(query),
              ),
            ),
            // 4
            Expanded(
              child: _buildResults(bloc),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildResults(CounterBloc bloc) {
    return StreamBuilder<int>(
      stream: bloc.outCounter,
      initialData: 0,
      builder: (context, snapshot) {

        // 1
        final results = snapshot.data;
        print("result----"+results.toString());

        if (results == null) {
          return Center(child: Text('Enter a location'));
        }

//        if (results.isEmpty) {
//          return Center(child: Text('No Results'));
//        }

        return _buildSearchResults(results);
      },
    );
  }

  Widget _buildSearchResults(int results) {
    return Text(results.toString());
    // 2
//    return ListView.separated(
//      //itemCount: results.length,
//      separatorBuilder: (BuildContext context, int index) => Divider(),
//      itemBuilder: (context, index) {
//        final location = results[index];
//        return ListTile(
//          title: Text(location.title),
//          onTap: () {
//            // 3
//            final locationBloc = BlocProvider.of<LocationBloc>(context);
//            locationBloc.selectLocation(location);
//
//            if (isFullScreenDialog) {
//              Navigator.of(context).pop();
//            }
//          },
//        );
//      },
//    );
  }





}

//to add
// bloc.incrementCounter.add(null);