import 'package:flutter/material.dart';
import 'package:stripe_payment/stripe_payment.dart';
import 'dart:async';
import 'package:flutter_learn/bloc/bloc_provider.dart';

import 'package:flutter_learn/bloc/application_bloc.dart';
import 'package:flutter_learn/screens/local_bloc_screen.dart';

class MainScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    final ApplicationBloc bloc = BlocProvider.of<ApplicationBloc>(context);


    //return LocationScreen();
    return StreamBuilder<int>(
      // 1
      stream: bloc.outCounter,
      initialData: 0,
      builder: (BuildContext context, AsyncSnapshot<int> snapshot) {
        final result = snapshot.data;


        // 2
        if (result == null) {
          return Container(
            child: Text("Null"),
          );
        }

        return LocalBlocForScreen();

        // This will be changed this later
//        return Container(
//          child: Row(
//            children: <Widget>[
//              Text("Value..."+result.toString()),
//              RaisedButton(
//                child: Text("++"),
//                onPressed: (){
//                  bloc.incrementCounter.add(null);
//                },
//              )
//            ],
//          ),
//        );
      },
    );
  }

}

//to add
// bloc.incrementCounter.add(null);