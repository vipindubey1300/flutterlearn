class Category {
  final int cat_id;
  final String name ;


  Category({
   this.cat_id,
    this.name
  });

  factory Category.fromJson(Map<String, dynamic> json) {
    return new Category(
      cat_id: json['cat_id'],
      name: json['name'].toString(),

    );
  }
}