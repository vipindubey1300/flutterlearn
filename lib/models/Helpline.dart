class Helpline {
  final String id;
  final String name;
  final String description;
  final String sub_categories_id;
  final String phone;



  Helpline({
   this.id,
    this.name,
    this.description,
    this.sub_categories_id,
    this.phone,
  });

  factory Helpline.fromJson(Map<String, dynamic> json) {
    return new Helpline(
      id: json['id'].toString(),
      name: json['name'],
      description: json['description'],
      sub_categories_id: json['sub_categories_id'].toString(),
      phone: json['phone'].toString(),

    );
  }
}