class SubcategoryDescription{
  final int sub_cat_id;
  final String name ;
  final String description ;


  SubcategoryDescription({
    this.sub_cat_id,
    this.name,
    this.description
  });

  factory SubcategoryDescription.fromJson(Map<String, dynamic> json) {
    return new SubcategoryDescription(
        sub_cat_id: json['sub_cat_id'],
        name: json['name'].toString(),
        description: json['description']

    );
  }
}
