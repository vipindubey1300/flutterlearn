class Models {
  final int model_id;
  final String name ;
  final String user_image;


  Models({
    this.model_id,
    this.name,
    this.user_image
  });

  factory Models.fromJson(Map<String, dynamic> json) {
    return new Models(
      model_id: json['model_id'],
      name: json['name'].toString(),
      user_image: json['user_image']

    );
  }
}