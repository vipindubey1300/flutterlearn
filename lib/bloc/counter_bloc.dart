
import 'dart:async';
import 'package:flutter_learn/bloc/bloc.dart';


class CounterBloc implements Bloc{
  int _counter;

  //
  // Stream to handle the counter
  //
  StreamController<int> _counterController = StreamController<int>();
  StreamSink<int> get inAdd => _counterController.sink;
  Stream<int> get outCounter => _counterController.stream;

  //
  // Stream to handle the action on the counter
  //
//  StreamController _actionController = StreamController();
//  StreamSink get incrementCounter => _actionController.sink;

  //
  // Constructor
  //
  CounterBloc(){
    _counter = 0;
//    _counterController.stream
//        .listen(handleLogic);
  }

  void dispose(){
  //  _actionController.close();
    _counterController.close();
  }

  void handleLogic(data){
    _counter = _counter + 1;
    inAdd.add(_counter);
  }
}