import 'package:flutter/material.dart';
import 'package:flutter_learn/models/models_list.dart';
import 'package:stripe_payment/stripe_payment.dart';
import 'dart:async';
import 'package:flutter_learn/bloc/bloc_provider.dart';
import 'package:flutter_learn/bloc/counter_bloc.dart';
import 'package:flutter_learn/api/api_repository.dart';
import 'package:flutter_learn/api/api_response.dart';


class ModelsBloc {
  ApiRepository _apiRepository;

  StreamController _modelsListController;

  StreamSink<ApiResponse<List<Models>>> get modelsListSink =>
      _modelsListController.sink;

  Stream<ApiResponse<List<Models>>> get movieListStream =>
      _modelsListController.stream;

  ModelsBloc() {
    _modelsListController = StreamController<ApiResponse<List<Models>>>();
    _apiRepository = ApiRepository();
    fetchMovieList();
  }

  fetchMovieList() async {
    modelsListSink.add(ApiResponse.loading('Fetching Popular Models'));
    try {
      List<Models> movies = await _apiRepository.fetchMovieList();
      modelsListSink.add(ApiResponse.completed(movies));
    } catch (e) {
      modelsListSink.add(ApiResponse.error(e.toString()));
      print(e);
    }
  }

  dispose() {
    _modelsListController?.close();
  }
}

/*

For consuming the various UI events and act accordingly, we are going to create a movie bloc.
 Its task is just to handle the “fetch movie list” event and adding the returned data to the Sink which then can be easily listened by our UI with the help of StreamBuilder.
The main part of blog comes here as we are going to handle all those exceptions that we created.
 The basic thing that we are doing here is to pass different states of our Data to our UI using the StreamController.


State: Loading -> Notifies the UI that our data is currently loading.
State: Completed -> Notifies the UI that our data is now successfully fetched and we can show it.
State: Error -> Notifies the UI that we got an error or exception while fetching our data.

 */