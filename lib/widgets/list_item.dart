import 'package:flutter/material.dart';
import 'package:flutter_learn/models/models_list.dart';
import 'package:stripe_payment/stripe_payment.dart';
import 'dart:async';
import 'package:flutter_learn/bloc/bloc_provider.dart';

import 'package:flutter_learn/bloc/application_bloc.dart';
import 'package:flutter_learn/screens/local_bloc_screen.dart';

//string is just a value in real we pass an object to display for the item in list


//Call this widget like
/*
     itemBuilder: (context, index) {
        final restaurant = results[index];
        return RestaurantTile(restaurant: restaurant);
      },
 */

class ModelsTile extends StatelessWidget {
  const ModelsTile({
    Key key,
    @required this.model,
  }) : super(key: key);

  final Models model;

  @override
  Widget build(BuildContext context) {
    return ListTile(

      title: Text(model.name),
      trailing: Image.network('https://www.d8bid.com/'+model.user_image,fit: BoxFit.fill,
        loadingBuilder:(BuildContext context, Widget child,ImageChunkEvent loadingProgress) {
          if (loadingProgress == null) return child;
          return Center(
            child: CircularProgressIndicator(
              value: loadingProgress.expectedTotalBytes != null ?
              loadingProgress.cumulativeBytesLoaded / loadingProgress.expectedTotalBytes
                  : null,
            ),
          );
        },
      ),
    );
  }
}
