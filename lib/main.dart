import 'package:flutter/material.dart';
import 'package:stripe_payment/stripe_payment.dart';
import 'dart:async';
import 'package:flutter_learn/screens/first.dart';
import 'package:flutter_learn/screens/movie_screen.dart';
import 'package:flutter_learn/bloc/bloc_provider.dart';
import 'package:flutter_learn/bloc/application_bloc.dart';


void main() => runApp(
    BlocProvider<ApplicationBloc>(
      bloc: ApplicationBloc(),
      child: MyApp(),
    )
);

//place a ApplicationBloc BLoC above the material app to store the app’s state

/*
Application blocs should be brodcast
 */


/*
BlocProvider<LocationBloc>(
  bloc: LocationBloc(),
  child: BlocProvider<FavoriteBloc>(
    bloc: FavoriteBloc(),
    child: MaterialApp(
      title: 'Restaurant Finder',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: MainScreen(),
    ),
  ),
);
 */

//Above is for multiple blocs

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Learn',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: ModelsScreen(),//MainScreen()
      routes: <String, WidgetBuilder>{
//        '/advicemyself':  (context) => new AdviceMyself(),
//        '/adviceothers':  (context) => new AdviceOthers(),

      },//routes of the screen
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);


  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;
  final StreamController<int> _streamController = StreamController<int>();

  @override
  void dispose(){
    _streamController.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Stream version of the Counter App')),
      body: Center(
        child: StreamBuilder<int>(
            stream: _streamController.stream,
            initialData: _counter,
            builder: (BuildContext context, AsyncSnapshot<int> snapshot){
              return Text('You hit me: ${snapshot.data} times');
            }
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.add),
        onPressed: (){
          _streamController.sink.add(++_counter);
        },
      ),
    );
  }
}

//FutureBuilder is used for one time response, like taking an image from Camera, getting data once from native platform (like fetching device battery), getting file reference, making an http request etc.

//On the other hand, StreamBuilder is used for fetching some data more than once, like listening for location update, playing a music, stopwatch, etc.


////USING FUTURE
//
//return Container(
//child: FutureBuilder(
//future: getPosts(),
//builder: (_, snapshot) {
//if (snapshot.connectionState == ConnectionState.waiting) {
//return Center(
//child: Column(
//mainAxisAlignment: MainAxisAlignment.center,
//crossAxisAlignment: CrossAxisAlignment.center,
//children: <Widget>[
//Text("Loading..."),
//SizedBox(
//height: 50.0,
//),
//CircularProgressIndicator()
//],
//),
//);
//} else {
//return ListView.builder(
//itemCount: snapshot.data.length,
//itemBuilder: (_, index) {
//return ListTile(
//title: Text(snapshot.data[index].data["title"]),
//onTap: () => navigateToDetail(snapshot.data[index]));
//},
//);
//}
//},
//),
//);


//StreamBuilder
//return Scaffold(
//body: Container(
//child: StreamBuilder<QuerySnapshot>(
//stream: Firestore.instance.collection("posts").snapshots(),
//builder:
//(BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
//if (snapshot.hasError) {
//return new Text('Error: ${snapshot.error}');
//}
//if (snapshot.connectionState == ConnectionState.waiting) {
//return Center(
//child: Column(
//mainAxisAlignment: MainAxisAlignment.center,
//crossAxisAlignment: CrossAxisAlignment.center,
//children: <Widget>[
//Text("Loading..."),
//SizedBox(
//height: 50.0,
//),
//CircularProgressIndicator()
//],
//),
//);
//} else {
//return ListView.builder(
//itemCount: snapshot.data.documents.length,
//itemBuilder: (_, index) {
//return Card(
//child: ListTile(
//title: Text(
//snapshot.data.documents[index].data["title"]),        // getting the data from firestore
//),
//);
//},
//);
//}
//},
//),
//),
//);
