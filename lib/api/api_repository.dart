import 'package:flutter_learn/api/api_base_helper.dart';
import 'package:flutter_learn/models/models_list.dart';

/*
We are going to use a Repository class which going to act as the intermediator and a layer of abstraction between the APIs and the BLOC.
 The task of the repository is to deliver movies data to the BLOC after fetching it from the API.
 */


class ApiRepository {
  //final String _apiKey = "Paste your api key here";

  ApiBaseHelper _helper = ApiBaseHelper();

  Future<List<Models>> fetchMovieList() async {
    //final response = await _helper.get("movie/popular?api_key=$_apiKey");
    //return Models.fromJson(response);
    final response = await _helper.get("models_list");
    //var responseJson = json.decode(response.body);
    return (response['result'] as List)
        .map((p) => Models.fromJson(p))
        .toList();
    return null;
  }
}