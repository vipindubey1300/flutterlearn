import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter_learn/models/models_list.dart';
//import 'package:movies_streams/models/movie_page_result.dart';
import 'package:http/http.dart' as http;


class TmdbApi {
//  static const String TMDB_API_KEY = "PUT YOUR KEY, HERE";
//  static const String baseUrl = 'api.themoviedb.org';
//  final String imageBaseUrl = 'http://image.tmdb.org/t/p/w185/';
  final _httpClient = new http.Client();

  ///
  /// Returns the list of movies/tv-show, based on criteria:
  /// [type]: movie or tv (show)
  /// [pageIndex]: page
  /// [minYear, maxYear]: release dates range
  /// [genre]: genre
  ///


  ///
  /// Returns the list of all genres
  ///
//  Future<Models> movieGenres({String type: "movie"}) async {
//    var uri = Uri.https(
//      baseUrl,
//      '3/genre/$type/list',
//      <String, String>{
//        'api_key': TMDB_API_KEY,
//        'language': 'en-US',
//      },
//    );
//
//    var response = await _getRequest(uri);
//    MovieGenresList list = MovieGenresList.fromJSON(json.decode(response));
//
//    return list;
//  }


  Future<List<Models>> _getModels() async {
    var response = await _httpClient.get('https://d8bid.com/api/models_list');
    print(response.statusCode.toString());
    if (response.statusCode == 200) {
      print("boddddy..." + response.body);
      var responseJson = json.decode(response.body);
      // print("Decode......"+responseJson.toString());//decode will remove all the  "" from the response

      if (!responseJson['error']) {
        //means if no error
        print(responseJson['response'] as List);
        return (responseJson['response'] as List)
            .map((p) => Models.fromJson(p))
            .toList();


        //map method will take step by step index item of list and store in p
        //and now p is a json   {cat_id: 1, name: Get Advice for myself}
        //get Models Object from this
//          return (responseJson['response'] as List)
//              .map((p) => Category.fromJson(p))
//              .toList();

      }
      else {
        return null;
      }
    }

    ///
    /// Routine to invoke the TMDB Web Server to get answers
    ///
//  Future<String> _getRequest(Uri uri) async {
//    var request = await _httpClient.getUrl(uri);
//    var response = await request.close();
//
//    return response.transform(utf8.decoder).join();
//  }
//}

  }

}

TmdbApi api = TmdbApi();


//USAGe
//import 'package:movies_streams/api/tmdb_api.dart';
/*
api.pagedList(pageIndex: pageIndex,
                       genre: _genre,
                       minYear: _minReleaseDate,
                       maxYear: _maxReleaseDate)
             .then((MoviePageResult fetchedPage) => _handleFetchedPage(fetchedPage, pageIndex));
*/
